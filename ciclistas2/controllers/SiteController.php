<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ciclista;
use app\models\Puerto;
use app\models\Etapa;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("count(dorsal) dorsal")->distinct(),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(dorsal) AS numeroCiclistas FROM ciclista",
            
        ]);
    } 

    public function actionConsulta1(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(dorsal) numeroCiclistas from ciclista;')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(dorsal) AS numeroCiclistas FROM ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numeroCiclistas'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(dorsal) AS numeroCiclistas FROM ciclista",
        ]);
    }
    
    public function actionConsulta2a (){
        
        $dataProvider =  new ActiveDataProvider ([
           'query'=>Ciclista::find()->select("count(dorsal) dorsal")->distinct()->where("nomequipo='Banesto'"),
            'pagination'=>[
                'pageSize'=> 5,
            ]
            
        ]);
        
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(dorsal) AS numeroCiclistas FROM ciclista WHERE nomequipo='Banesto'",
            
        ]);
        
    }
    public function actionConsulta2 (){
        $numero = Yii::$app->db
                ->createCommand("SELECT count(dorsal) numeroCiclistas FROM ciclista WHERE nomequipo = 'Banesto'")
                ->queryScalar();
        
        $dataProvider =  new SqlDataProvider ([
           'sql'=>"SELECT COUNT(dorsal) AS numeroCiclistas FROM ciclista WHERE nomequipo='Banesto'",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=> 5,
            ]
            
        ]);
        
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numeroCiclistas'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(dorsal) AS numeroCiclistas FROM ciclista WHERE nomequipo='Banesto'",
            
        ]);
        
    }
    
    


     public function actionConsulta3a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("AVG(edad) edad") ->distinct(),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) AS edadMedia FROM ciclista;",
      
            
        ]);
 
} 
    public function actionConsulta3(){
        
          $numero = Yii::$app->db
                    ->createCommand("SELECT AVG(edad) edadMedia FROM ciclista;")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT AVG(edad) AS edadMedia FROM ciclista;",
            'totalCount'=>$numero,
            'pagination'=>[
            'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edadMedia'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) AS edadMedia FROM ciclista;",
            
        ]);
    }  
    public function actionConsulta4a(){

        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("avg(edad) edad") ->distinct() ->where("nomequipo='Banesto'"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"SELECT AVG(edad) AS edadMedia FROM ciclista WHERE nomequipo='Banesto'",
      
            
        ]);
 
} 
     
    public function actionConsulta4(){
          $numero = Yii::$app->db
                    ->createCommand("SELECT AVG(edad) edadMedia FROM ciclista WHERE nomequipo='Banesto';")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT AVG(edad) AS edadMedia FROM ciclista WHERE nomequipo='Banesto'",
            'totalCount'=>$numero,
            'pagination'=>[
            'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edadMedia'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"SELECT AVG(edad) AS edadMedia FROM ciclista WHERE nomequipo='Banesto'",
            
        ]);
    } 
     public function actionConsulta5a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("nomequipo,avg(edad) edad")->groupBy("nomequipo"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT DISTINCT nomequipo,AVG(edad) AS edadMedia FROM ciclista GROUP BY nomequipo" , 
      
            
        ]);
 
} 
    
    public function actionConsulta5(){
          $numero = Yii::$app->db
                    ->createCommand("SELECT count(distinct nomequipo),avg(edad) edadMedia FROM ciclista GROUP BY nomequipo")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT nomequipo,AVG(edad) AS edadMedia FROM ciclista GROUP BY nomequipo",
            'totalCount'=>$numero,
            'pagination'=>[
            'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','edadMedia'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT DISTINCT nomequipo,AVG(edad) AS edadMedia FROM ciclista GROUP BY nomequipo",
            
        ]);
    }  
    
    public function actionConsulta6a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("nomequipo,count(nombre) nombre") ->distinct() ->groupBy("nomequipo"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','nombre'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT DISTINCT nomequipo,count(nombre) AS numero_ciclistas FROM ciclista GROUP BY nomequipo", 
      
            
        ]);
 
    } 

    
    
    public function actionConsulta6(){
  
          $numero = Yii::$app->db
                    ->createCommand("SELECT count(nomequipo),count(nombre) numero_ciclistas FROM ciclista GROUP BY nomequipo")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT nomequipo,count(nombre) AS numero_ciclistas FROM ciclista GROUP BY nomequipo",
            'totalCount'=>$numero,
            'pagination'=>[
            'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','numero_ciclistas'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT DISTINCT nomequipo,count(nombre) AS numero_ciclistas FROM ciclista GROUP BY nomequipo",
            
        ]);
    }  
     public function actionConsulta7a(){

        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find() ->select("count(nompuerto) nompuerto")
                                        ->distinct()
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(nompuerto) AS num_puertos FROM puerto", 
      
            
        ]);
 
    } 
   
    
   
    public function actionConsulta7(){

        $numero = Yii::$app->db
                    ->createCommand("SELECT count(nompuerto) num_puertos FROM ciclista")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT COUNT(nompuerto) AS num_puertos FROM puerto",
            'totalCount'=>$numero,
            'pagination'=>[
            'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['num_puertos'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(nompuerto) AS num_puertos FROM puerto",
            
        ]);
    } 
    
     public function actionConsulta8a(){

        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find() ->select("count(nompuerto) nompuerto") ->distinct() ->where("altura>1500"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(nompuerto) AS num_puertos FROM puerto WHERE altura>1500", 
      
            
        ]);
 
    } 
    
    
    
    
    public function actionConsulta8(){

         $numero = Yii::$app->db
                    ->createCommand("SELECT count(distinct nompuerto) num_puertos FROM puerto WHERE altura>1500")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT COUNT(nompuerto) AS num_puertos FROM puerto WHERE altura>1500",
            'totalCount'=>$numero,
            'pagination'=>[
            'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['num_puertos'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(nompuerto) AS num_puertos FROM puerto WHERE altura>1500",
            
        ]);
    } 
    
   public function actionConsulta9a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("nomequipo,count(nombre) nombre") ->groupBy("nomequipo") ->having("count(nombre)>4"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','nombre'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo,COUNT(nombre) AS ciclistas FROM ciclista GROUP BY nomequipo HAVING ciclistas>4", 
      
            
        ]);
 
    } 
   
    public function actionConsulta9(){

         $numero = Yii::$app->db
                    ->createCommand("SELECT count(distinct nomequipo),count(nombre) ciclistas FROM ciclista GROUP BY nomequipo HAVING ciclistas > 4")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT nomequipo,COUNT(nombre) AS ciclistas FROM ciclista GROUP BY nomequipo HAVING ciclistas>4",
            'totalCount'=>$numero,
            'pagination'=>[
            'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','ciclistas'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo,COUNT(nombre) AS ciclistas FROM ciclista GROUP BY nomequipo HAVING ciclistas>4",
            
        ]);
    } 
    
    public function actionConsulta10a(){

        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("nomequipo,count(nombre) nombre") ->distinct() ->where("edad BETWEEN 28 AND 32") ->groupBy("nomequipo") ->having("count(nombre)"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','nombre'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo,COUNT(nombre) AS ciclistas FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING ciclistas>4;", 
      
            
        ]);
 
    }
    
    public function actionConsulta10(){
         $numero = Yii::$app->db
                    ->createCommand("SELECT count(distinct nomequipo),count(nombre) ciclistas FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING ciclistas>4")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT nomequipo,COUNT(nombre) AS ciclistas FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING ciclistas>4;",
            'totalCount'=>$numero,
            'pagination'=>[
            'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','ciclistas'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo,COUNT(nombre) AS ciclistas FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING ciclistas>4",
            
        ]);
    } 
    
      public function actionConsulta11a(){
       $dataProvider = new ActiveDataProvider([
            'query' => Ciclista,Etapa::find() ->select("nombre,count(numetapa) numetapa") ->distinct() ->where("ciclista.dorsal=etapa.dorsal") ->groupBy ("nombre"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','numetapa'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT DISTINCT nombre,COUNT(numetapa) AS etapas_ganadas FROM ciclista,etapa WHERE ciclista.dorsal=etapa.dorsal GROUP BY nombre", 
        ]);
    }
    
    
    public function actionConsulta11(){
         $numero = Yii::$app->db
                    ->createCommand("SELECT count(distinct nombre),count(numetapa) etapas_ganadas FROM ciclista,etapa WHERE ciclista.dorsal=etapa.dorsal GROUP BY nombre")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT nombre,COUNT(numetapa) as etapas_ganadas FROM ciclista,etapa WHERE ciclista.dorsal=etapa.dorsal GROUP BY nombre",
            'totalCount'=>$numero,
            'pagination'=>[
            'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','etapas_ganadas'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT DISTINCT nombre,COUNT(numetapa) AS etapas_ganadas FROM ciclista,etapa WHERE ciclista.dorsal=etapa.dorsal GROUP BY nombre",
            
        ]);
    } 

   
    
    public function actionConsulta12a(){
       $dataProvider = new ActiveDataProvider([
            'query' => Ciclista,Etapa::find() ->select("dorsal,count(numetapa) numetapa") ->distinct() ->where("ciclista.dorsal=etapa.dorsal") ->groupBy ("nombre") ->having("count(numetapa)"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','numetapa'],
            "titulo"=>"Consulta 12 con Active Record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT DISTINCT ciclista.dorsal,COUNT(numetapa) AS etapas_ganadas FROM ciclista,etapa WHERE ciclista.dorsal=etapa.dorsal GROUP BY nombre HAVING etapas_ganadas>1", 
        ]);
    }

    
    public function actionConsulta12(){
         $numero = Yii::$app->db
                    ->createCommand("SELECT count(distinct ciclista.dorsal),count(numetapa) etapas_ganadas FROM ciclista,etapa WHERE ciclista.dorsal=etapa.dorsal GROUP BY nombre HAVING count(numetapa)>1")
                    ->queryScalar();
                
                
        $dataProvider = new SqlDataProvider([
            'sql' =>"SELECT ciclista.dorsal,COUNT(numetapa) AS etapas_ganadas FROM ciclista,etapa WHERE ciclista.dorsal=etapa.dorsal GROUP BY nombre HAVING etapas_ganadas>1",
            'totalCount'=>$numero,
            'pagination'=>[
            'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','etapas_ganadas'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT ciclista.dorsal,COUNT(numetapa) AS etapas_ganadas FROM ciclista,etapa WHERE ciclista.dorsal=etapa.dorsal GROUP BY nombre HAVING etapas_ganadas>1",
            
        ]);
    } 

    
}

